# frozen_string_literal: true

# Performs requests to Google Vision API and image processing in the background
# and updates related Picture models when done
class FaceRecognitionJob < ApplicationJob
  queue_as :default

  def perform(picture_id:, image_path:)
    api = VisionAPI.new
    processor = ImageProcessor.new(image_path)

    result = processor.process_faces(api.face_recognition_metadata(image_path))

    picture = Picture.find(picture_id)

    picture.update!(
      processed_image: File.new(result, 'r')
    )
  end
end
