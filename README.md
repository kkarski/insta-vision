Readme
===
A simple application featuring image uploads with Google Cloud Vision support.

Ruby version
---
2.4.3

System dependencies
---
* redis-server
* postgresql
* libpq-dev
* imagemagick
* libmagicwand-dev
* nodejs

Configuration
---
1. create .env file in project root (cp .env.example .env)
2. fill in required secrets

Database creation
---
1. bundle exec rails db:create
2. bundle exec rails db:migrate

Database initialization
---

How to run the test suite
---
* bundle exec rails db:test:prepare
* bundle exec rails t

Services (job queues, cache servers, search engines, etc.)
---
* bundle exec sidekiq (can be daemonized)

Deployment instructions
---
1. fill in deployment secrets in .env
2. bundle exec cap [stage] deploy
