# Helper methods to be used in views and controllers
module ApplicationHelper
  # override Rails defaults with Bootstrap-specific alerts
  def flash_class(level)
    alert_class_common = 'alert alert-dismissible fade in'.freeze

    case level
    when 'notice' then "#{alert_class_common} alert-info"
    when 'success' then "#{alert_class_common} alert-success"
    when 'error' then "#{alert_class_common} alert-danger"
    when 'alert' then "#{alert_class_common} alert-warning"
    else alert_class_common
    end
  end
end
