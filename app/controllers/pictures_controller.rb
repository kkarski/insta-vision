# frozen_string_literal: true

# Controller class for the Picture model
class PicturesController < ApplicationController
  # GET /pictures
  def index
    @options = {
      current_page: params[:page] || 1,
      layout: params[:view] || 'grid'
    }

    @pictures = Picture.order(created_at: :desc).page(params[:page])
  end

  # GET /pictures/new
  def new
    @picture = Picture.new
  end

  # POST /pictures
  def create
    @picture = Picture.new(picture_params)

    respond_to do |format|
      if @picture.save
        PictureManager.send_to_processing(@picture)

        format.html do
          flash[:success] = 'Picture was successfully uploaded.'

          redirect_to action: :index
        end
      else
        format.html do
          flash[:error] = 'Picture was not uploaded.'

          render :new
        end
      end
    end
  end

  private

  def set_picture
    @picture = Picture.find(params[:id])
  end

  def picture_params
    params.require(:picture).permit(
      :name,
      :description,
      :image
    )
  end
end
