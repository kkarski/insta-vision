# frozen_string_literal: true

require 'google/cloud/vision'

# Service class used for connecting with Google Vision API
class VisionAPI
  def initialize
    @vision = Google::Cloud::Vision.new(
      project: Rails.application.secrets.google_vision_project,
      keyfile: Rails.application.secrets.google_vision_keyfile
    )
  end

  def face_recognition_metadata(path)
    image = @vision.image(path)

    image.faces
  end
end
