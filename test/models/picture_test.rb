# frozen_string_literal: true

require 'test_helper'

class PictureTest < ActiveSupport::TestCase
  setup do
    @picture = pictures(:one)
  end

  test 'it should be valid' do
    assert @picture.valid?
  end

  test 'it should not be valid without a name' do
    @picture.name = nil

    refute @picture.valid?
  end

  test 'it should have image attached' do
    @picture.image = nil

    refute @picture.valid?
  end
end
