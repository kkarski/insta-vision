# frozen_string_literal: true

# Module for handling various picture-related tasks
module PictureManager
  def self.send_to_processing(picture)
    FaceRecognitionJob.perform_later(
      picture_id: picture.id,
      image_path: picture.image.path
    )
  end
end
