# frozen_string_literal: true

require 'rmagick'

# Service class for drawing polygons around recognized faces in pictures
class ImageProcessor
  DRAW_STROKE = 'green'
  DRAW_STROKE_WIDTH = 5
  DRAW_FILL_OPACITY = 0

  def initialize(input_file_path)
    @input_file_path = input_file_path
    @output_file_path = File.join(
      File.dirname(input_file_path),
      "#{File.basename(input_file_path, '.*')}_processed#{File.extname(input_file_path)}"
    )
  end

  def process_faces(metadata)
    @output_file_path if draw_polygons(metadata)
  end

  private

  # Draws polygons around faces recognized by Google Vision API
  def draw_polygons(metadata)
    image = Magick::Image.read(@input_file_path).first

    metadata.each do |face|
      draw_rectangle_around_face(face, image)
    end

    image.write(@output_file_path)
  end

  # Use RMagick gem to draw rectangles
  def draw_rectangle_around_face(face, image)
    draw = Magick::Draw.new
    draw.stroke = DRAW_STROKE
    draw.stroke_width DRAW_STROKE_WIDTH
    draw.fill_opacity DRAW_FILL_OPACITY

    draw.rectangle(
      face.bounds.face[0].x.to_i,
      face.bounds.face[0].y.to_i,
      face.bounds.face[2].x.to_i,
      face.bounds.face[2].y.to_i
    )

    draw.draw(image)
  end
end
