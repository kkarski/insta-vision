# frozen_string_literal: true

require 'test_helper'

class PicturesControllerTest < ActionDispatch::IntegrationTest
  test 'should get index' do
    get pictures_url

    assert_response :ok
  end

  test 'should get new' do
    get new_picture_url

    assert_response :ok
  end

  test 'should create picture' do
    img = fixture_file_upload('files/time_travel.png', 'image/png')

    PictureManager.stub(:send_to_processing, true) do
      assert_difference('Picture.count') do
        post pictures_url, params: {
          picture: {
            name: 'Test picture',
            description: 'This is a test',
            image: img
          }
        }
      end
    end

    assert_redirected_to pictures_url
  end
end
