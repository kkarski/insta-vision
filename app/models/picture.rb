# Class for holding pictures using Paperclip gem
class Picture < ApplicationRecord
  DEFAULT_STYLES = {
    thumb: '100x100>',
    small: '200x200>',
    medium: '300x300>'
  }.freeze

  has_attached_file :image, styles: DEFAULT_STYLES
  has_attached_file :processed_image, styles: DEFAULT_STYLES

  validates :name, presence: true
  validates :description, length: { maximum: 255, allow_blank: true }

  validates_attachment :image,
                       presence: true,
                       content_type: {
                         content_type: %r{\Aimage\/.*\z}
                       }
  validates_attachment :processed_image,
                       content_type: {
                         content_type: %r{\Aimage\/.*\z}
                       },
                       allow_nil: true
end
