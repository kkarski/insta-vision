# frozen_string_literal: true

Rails.application.routes.draw do
  concern :paginatable do
    get '((page/:page)(/view/:view))', action: :index, on: :collection
  end

  resources :pictures,
            concerns: %i[paginatable],
            only: %i[index new create]

  get '/about', to: 'articles#about', as: 'about'
  root to: 'articles#welcome'
end
