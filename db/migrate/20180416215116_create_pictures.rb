class CreatePictures < ActiveRecord::Migration[5.1]
  def change
    create_table :pictures do |t|
      t.string :name, null: false
      t.string :description, null: true, default: nil

      t.timestamps
    end

    add_attachment :pictures, :image

    add_index :pictures, :name
  end
end
