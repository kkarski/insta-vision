class AddProcessedImageToPictures < ActiveRecord::Migration[5.1]
  def change
    add_attachment :pictures, :processed_image
  end
end
