settings = YAML.safe_load(File.read('config/capistrano_secrets.yml'))

server settings['production']['server']['name'],
       user: settings['production']['server']['user'],
       roles: settings['production']['server']['roles']

set :branch, settings['production']['git']['branch']
# Deployment directory
set :deploy_to, settings['production']['git']['directory']
# Set Rails environment
set :rails_env, 'production'
