# frozen_string_literal: true

# Controller class for "static" content
class ArticlesController < ApplicationController
  # GET /
  def welcome; end

  # GET /articles/about
  def about; end
end
