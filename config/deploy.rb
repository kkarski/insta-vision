require 'yaml'

settings = YAML.safe_load(File.read('config/capistrano_secrets.yml'))

set :application, settings['global']['application']
set :repo_url, settings['global']['repository']

# Default value for :pty is false
set :pty, true

# Default value for :log_level is :debug
set :log_level, :debug

# set :passenger_restart_with_sudo, true
set :use_sudo, true

# Default value for :linked_files is []
set :linked_files, fetch(:linked_files, []).push('.env')

# Default value for linked_dirs is []
set :linked_dirs, fetch(:linked_dirs, []).push('log', 'tmp/pids', 'tmp/cache', 'tmp/sockets')

# Default value for keep_releases is 5
set :keep_releases, 5

# Take down Apache, Passenger and effectively whole application - in case of panic
desc 'Stop application'
task :down do
  on roles(:all) do |_host|
    within current_path do
      execute :sudo, 'service nginx stop'
    end
  end
end

# Start application after panic is resolved
desc 'Start application'
task :up do
  on roles(:all) do |_host|
    within current_path do
      execute :sudo, 'service nginx start'
    end
  end
end
